#pragma once

#include <string>
#include <fstream>
#include <vector>

#ifdef LIBSCHED2CAL_EXPORTS
#define LIBSCHED2CAL_API __declspec(dllexport)
#else
#define LIBSCHED2CAL_API __declspec(dllimport)
#endif

class Datum {
public:
	Datum();
	Datum(int day, int month, int year);
	Datum(std::string datestring);
	virtual ~Datum();

	std::string GetDatumStr();
	std::string GetDatumCal();
	int* GetDatum();

	Datum operator+(int num);

private:
	int date[3] = {0,0,0};
};

enum LIBSCHED2CAL_API TriggerType { tt_minute, tt_hour, tt_day };

typedef struct LIBSCHED2CAL_API CalendarReminder
{
public:
	CalendarReminder(unsigned int t, TriggerType tt);
	std::string getReminder();

private:
	const char* beg = "BEGIN:VALARM";
	std::string trg = "TRIGGER;VALUE=DURATION:";
	const char* end = "REPEAT:1\nDURATION:PT15M\nACTION:DISPLAY\nDESCRIPTION:Reminder\nEND:VALARM";
};

class CalendarEvent
{
public:
	CalendarEvent();
	CalendarEvent(std::string _sum, Datum d1, int h1, int m1, Datum d2, int h2, int m2);
	virtual ~CalendarEvent();

	void addSummary(std::string _sum) { sum.append(_sum); }
	void addStartTime(Datum d, int hh, int mm);
	void addEndTime(Datum d, int hh, int mm);
	void addReminder(unsigned int t, TriggerType tt);
	void addReminder(CalendarReminder r);
	void writeEvent(std::ofstream* file);

private:
	const char* beg = "BEGIN:VEVENT";
	std::string sum = "SUMMARY:";
	std::string tst = "DTSTART;TZID=Europe/Berlin:";
	std::string ted = "DTEND;TZID=Europe/Berlin:";
	const char* sts = "STATUS:CONFIRMED";
	std::vector<CalendarReminder> reminder;
	const char* end = "END:VEVENT";
};

class Calendar
{
public:
	Calendar();
	Calendar(CalendarEvent _evt);
	Calendar(std::vector<CalendarEvent> _evtList);
	virtual ~Calendar();

	void addCalendarEvent(CalendarEvent _evt);
	void addCalendarEvent(std::vector<CalendarEvent> _evtList);
	void writeCalendar(const char* filepath);

private:
	const char* beg = "BEGIN:VCALENDAR";
	const char* ver = "VERSION:2.0";
	const char* sca = "CALSCALE:GREGORIAN";
	std::vector<CalendarEvent> evt;
	const char* end = "END:VCALENDAR";
};

LIBSCHED2CAL_API void getNames(const char* dpPath, std::vector<std::string>* names);
LIBSCHED2CAL_API int makeCalendar(const char* dpPath, const char* name, std::string savePath, std::string kw, std::string* fp, std::vector<CalendarReminder>* r);
