#pragma once

#include "libsched2cal.h"

namespace CppCLRWinFormsProject {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:

		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			this->kw = gcnew System::String("");
			this->cb_TriggerType->SelectedIndex = 0;
			this->t_reminders = gcnew System::Data::DataTable("Reminders");
			this->t_reminders->Columns->Add(gcnew System::Data::DataColumn("Reminders", System::String::typeid));
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ l_DPPath;
	private: System::Windows::Forms::Label^ l_Name;
	private: System::Windows::Forms::Label^ l_ICSPath;

	private: System::Windows::Forms::TextBox^ tb_DPPath;
	private: System::Windows::Forms::TextBox^ tb_ICSPath;
	private: System::Windows::Forms::Button^ b_ICSPath;
	private: System::Windows::Forms::Button^ b_DPPath;
	private: System::Windows::Forms::Button^ b_Create;
	private: System::Windows::Forms::FolderBrowserDialog^ folderBrowserDialog1;
	private: System::Windows::Forms::OpenFileDialog^ openFileDialog1;
	private: System::Windows::Forms::SaveFileDialog^ saveFileDialog1;

	private: System::String^ kw;
	private: System::Windows::Forms::ListBox^ lb_Debug;
	private: System::Windows::Forms::ComboBox^ cb_Names;
	private: System::Windows::Forms::Label^ l_reminder;
	private: System::Windows::Forms::ListBox^ lb_reminder;
	private: System::Windows::Forms::NumericUpDown^ nud_counter;
	private: System::Windows::Forms::ComboBox^ cb_TriggerType;
	private: System::Windows::Forms::Button^ b_addReminder;

	private: System::Windows::Forms::Button^ b_delReminder;

	private: System::Data::DataTable^ t_reminders;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->l_DPPath = (gcnew System::Windows::Forms::Label());
			this->l_Name = (gcnew System::Windows::Forms::Label());
			this->l_ICSPath = (gcnew System::Windows::Forms::Label());
			this->tb_DPPath = (gcnew System::Windows::Forms::TextBox());
			this->tb_ICSPath = (gcnew System::Windows::Forms::TextBox());
			this->b_ICSPath = (gcnew System::Windows::Forms::Button());
			this->b_DPPath = (gcnew System::Windows::Forms::Button());
			this->b_Create = (gcnew System::Windows::Forms::Button());
			this->folderBrowserDialog1 = (gcnew System::Windows::Forms::FolderBrowserDialog());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->lb_Debug = (gcnew System::Windows::Forms::ListBox());
			this->cb_Names = (gcnew System::Windows::Forms::ComboBox());
			this->l_reminder = (gcnew System::Windows::Forms::Label());
			this->lb_reminder = (gcnew System::Windows::Forms::ListBox());
			this->nud_counter = (gcnew System::Windows::Forms::NumericUpDown());
			this->cb_TriggerType = (gcnew System::Windows::Forms::ComboBox());
			this->b_addReminder = (gcnew System::Windows::Forms::Button());
			this->b_delReminder = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->nud_counter))->BeginInit();
			this->SuspendLayout();
			// 
			// l_DPPath
			// 
			this->l_DPPath->AutoSize = true;
			this->l_DPPath->Location = System::Drawing::Point(41, 19);
			this->l_DPPath->Margin = System::Windows::Forms::Padding(10);
			this->l_DPPath->Name = L"l_DPPath";
			this->l_DPPath->Size = System::Drawing::Size(197, 20);
			this->l_DPPath->TabIndex = 0;
			this->l_DPPath->Text = L"Dateipfad zum Dienstplan:";
			// 
			// l_Name
			// 
			this->l_Name->AutoSize = true;
			this->l_Name->Location = System::Drawing::Point(183, 59);
			this->l_Name->Margin = System::Windows::Forms::Padding(10);
			this->l_Name->Name = L"l_Name";
			this->l_Name->Size = System::Drawing::Size(55, 20);
			this->l_Name->TabIndex = 1;
			this->l_Name->Text = L"Name:";
			// 
			// l_ICSPath
			// 
			this->l_ICSPath->AutoSize = true;
			this->l_ICSPath->Location = System::Drawing::Point(19, 99);
			this->l_ICSPath->Margin = System::Windows::Forms::Padding(10);
			this->l_ICSPath->Name = L"l_ICSPath";
			this->l_ICSPath->Size = System::Drawing::Size(219, 20);
			this->l_ICSPath->TabIndex = 2;
			this->l_ICSPath->Text = L"Dateipfad zur Kalender-Datei:";
			// 
			// tb_DPPath
			// 
			this->tb_DPPath->Location = System::Drawing::Point(258, 16);
			this->tb_DPPath->Margin = System::Windows::Forms::Padding(10);
			this->tb_DPPath->Name = L"tb_DPPath";
			this->tb_DPPath->Size = System::Drawing::Size(500, 26);
			this->tb_DPPath->TabIndex = 0;
			// 
			// tb_ICSPath
			// 
			this->tb_ICSPath->Location = System::Drawing::Point(258, 96);
			this->tb_ICSPath->Margin = System::Windows::Forms::Padding(10);
			this->tb_ICSPath->Name = L"tb_ICSPath";
			this->tb_ICSPath->Size = System::Drawing::Size(500, 26);
			this->tb_ICSPath->TabIndex = 3;
			// 
			// b_ICSPath
			// 
			this->b_ICSPath->AutoSize = true;
			this->b_ICSPath->Location = System::Drawing::Point(778, 94);
			this->b_ICSPath->Margin = System::Windows::Forms::Padding(10);
			this->b_ICSPath->Name = L"b_ICSPath";
			this->b_ICSPath->Size = System::Drawing::Size(75, 30);
			this->b_ICSPath->TabIndex = 4;
			this->b_ICSPath->Text = L"�ffnen";
			this->b_ICSPath->UseVisualStyleBackColor = true;
			this->b_ICSPath->Click += gcnew System::EventHandler(this, &Form1::b_ICSPath_Click);
			// 
			// b_DPPath
			// 
			this->b_DPPath->AutoSize = true;
			this->b_DPPath->Location = System::Drawing::Point(778, 16);
			this->b_DPPath->Margin = System::Windows::Forms::Padding(10);
			this->b_DPPath->Name = L"b_DPPath";
			this->b_DPPath->Size = System::Drawing::Size(75, 30);
			this->b_DPPath->TabIndex = 1;
			this->b_DPPath->Text = L"�ffnen";
			this->b_DPPath->UseVisualStyleBackColor = true;
			this->b_DPPath->Click += gcnew System::EventHandler(this, &Form1::b_DPPath_Click);
			// 
			// b_Create
			// 
			this->b_Create->AutoSize = true;
			this->b_Create->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 20));
			this->b_Create->Location = System::Drawing::Point(508, 238);
			this->b_Create->Margin = System::Windows::Forms::Padding(10);
			this->b_Create->Name = L"b_Create";
			this->b_Create->Size = System::Drawing::Size(272, 68);
			this->b_Create->TabIndex = 5;
			this->b_Create->Text = L"Speichern";
			this->b_Create->UseVisualStyleBackColor = true;
			this->b_Create->Click += gcnew System::EventHandler(this, &Form1::b_Create_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->Filter = L"Dienstplan (*.pdf)|*.pdf";
			// 
			// lb_Debug
			// 
			this->lb_Debug->FormattingEnabled = true;
			this->lb_Debug->ItemHeight = 20;
			this->lb_Debug->Location = System::Drawing::Point(23, 319);
			this->lb_Debug->Name = L"lb_Debug";
			this->lb_Debug->Size = System::Drawing::Size(830, 204);
			this->lb_Debug->TabIndex = 99;
			this->lb_Debug->TabStop = false;
			// 
			// cb_Names
			// 
			this->cb_Names->FormattingEnabled = true;
			this->cb_Names->Location = System::Drawing::Point(258, 56);
			this->cb_Names->Margin = System::Windows::Forms::Padding(10);
			this->cb_Names->Name = L"cb_Names";
			this->cb_Names->Size = System::Drawing::Size(230, 28);
			this->cb_Names->TabIndex = 2;
			// 
			// l_reminder
			// 
			this->l_reminder->AutoSize = true;
			this->l_reminder->Location = System::Drawing::Point(50, 139);
			this->l_reminder->Margin = System::Windows::Forms::Padding(10);
			this->l_reminder->Name = L"l_reminder";
			this->l_reminder->Size = System::Drawing::Size(188, 20);
			this->l_reminder->TabIndex = 100;
			this->l_reminder->Text = L"Erinnerungen hinzuf�gen";
			// 
			// lb_reminder
			// 
			this->lb_reminder->FormattingEnabled = true;
			this->lb_reminder->ItemHeight = 20;
			this->lb_reminder->Location = System::Drawing::Point(258, 142);
			this->lb_reminder->Margin = System::Windows::Forms::Padding(10);
			this->lb_reminder->Name = L"lb_reminder";
			this->lb_reminder->Size = System::Drawing::Size(230, 164);
			this->lb_reminder->TabIndex = 101;
			this->lb_reminder->TabStop = false;
			// 
			// nud_counter
			// 
			this->nud_counter->Location = System::Drawing::Point(508, 142);
			this->nud_counter->Margin = System::Windows::Forms::Padding(10);
			this->nud_counter->Name = L"nud_counter";
			this->nud_counter->Size = System::Drawing::Size(120, 26);
			this->nud_counter->TabIndex = 102;
			// 
			// cb_TriggerType
			// 
			this->cb_TriggerType->FormattingEnabled = true;
			this->cb_TriggerType->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"Minuten", L"Stunden", L"Tage" });
			this->cb_TriggerType->Location = System::Drawing::Point(648, 142);
			this->cb_TriggerType->Margin = System::Windows::Forms::Padding(10);
			this->cb_TriggerType->Name = L"cb_TriggerType";
			this->cb_TriggerType->Size = System::Drawing::Size(132, 28);
			this->cb_TriggerType->TabIndex = 103;
			// 
			// b_addReminder
			// 
			this->b_addReminder->AutoSize = true;
			this->b_addReminder->Location = System::Drawing::Point(508, 188);
			this->b_addReminder->Margin = System::Windows::Forms::Padding(10);
			this->b_addReminder->Name = L"b_addReminder";
			this->b_addReminder->Size = System::Drawing::Size(120, 30);
			this->b_addReminder->TabIndex = 104;
			this->b_addReminder->Text = L"Hinzuf�gen";
			this->b_addReminder->UseVisualStyleBackColor = true;
			this->b_addReminder->Click += gcnew System::EventHandler(this, &Form1::b_addReminder_Click);
			// 
			// b_delReminder
			// 
			this->b_delReminder->AutoSize = true;
			this->b_delReminder->Location = System::Drawing::Point(648, 190);
			this->b_delReminder->Margin = System::Windows::Forms::Padding(10);
			this->b_delReminder->Name = L"b_delReminder";
			this->b_delReminder->Size = System::Drawing::Size(132, 30);
			this->b_delReminder->TabIndex = 105;
			this->b_delReminder->Text = L"Entfernen";
			this->b_delReminder->UseVisualStyleBackColor = true;
			this->b_delReminder->Click += gcnew System::EventHandler(this, &Form1::b_delReminder_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(9, 20);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(874, 540);
			this->Controls->Add(this->b_delReminder);
			this->Controls->Add(this->b_addReminder);
			this->Controls->Add(this->cb_TriggerType);
			this->Controls->Add(this->nud_counter);
			this->Controls->Add(this->lb_reminder);
			this->Controls->Add(this->l_reminder);
			this->Controls->Add(this->cb_Names);
			this->Controls->Add(this->lb_Debug);
			this->Controls->Add(this->b_Create);
			this->Controls->Add(this->b_DPPath);
			this->Controls->Add(this->b_ICSPath);
			this->Controls->Add(this->tb_ICSPath);
			this->Controls->Add(this->tb_DPPath);
			this->Controls->Add(this->l_ICSPath);
			this->Controls->Add(this->l_Name);
			this->Controls->Add(this->l_DPPath);
			this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
			this->Name = L"Form1";
			this->Text = L"sched2cal";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->nud_counter))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void b_DPPath_Click(System::Object^ sender, System::EventArgs^ e) {
		if (this->openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			this->tb_DPPath->Text = this->openFileDialog1->FileName;


			System::String^ workstring = this->tb_DPPath->Text;
			array<System::String^>^ splitstring = workstring->Split('\\');
			this->kw = splitstring[splitstring->Length - 1];
			
			if (this->tb_ICSPath->Text == "")
			{
				array<System::String^>^ splitpath = gcnew array<System::String^>(splitstring->Length - 1);
				for (size_t i = 0; i < splitstring->Length - 1; i++)
				{
					splitpath[i] = splitstring[i];
				}
				this->tb_ICSPath->Text = String::Join(gcnew System::String("\\"), splitpath);
			}
			
			std::vector<std::string> nameList;
			const char* dpPath = (const char*)Marshal::StringToHGlobalAnsi(this->tb_DPPath->Text).ToPointer();
			getNames(dpPath, &nameList);

			this->cb_Names->Items->Clear();
			for (std::string s: nameList)
			{
				this->cb_Names->Items->Add(gcnew String((const char*)s.c_str()));
			}
			
		}
	}

	private: System::Void b_ICSPath_Click(System::Object^ sender, System::EventArgs^ e) {
		if (this->folderBrowserDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			this->tb_ICSPath->Text = this->folderBrowserDialog1->SelectedPath;
		}
	}

	private: System::Void b_Create_Click(System::Object^ sender, System::EventArgs^ e) {
		this->lb_Debug->Items->Clear();
		bool noFailure = true;
		this->tb_DPPath->BackColor = Color::White;
		this->cb_Names->BackColor = Color::White;
		this->tb_ICSPath->BackColor = Color::White;
		if (this->tb_DPPath->Text == "")
		{
			noFailure = false;
			this->tb_DPPath->BackColor = System::Drawing::Color::Red;
		}
		if (this->cb_Names->Text == "")
		{
			noFailure = false;
			this->cb_Names->BackColor = System::Drawing::Color::Red;
		}
		if (this->tb_ICSPath->Text == "")
		{
			noFailure = false;
			this->tb_ICSPath->BackColor = System::Drawing::Color::Red;
		}
		if (noFailure)
		{
			const char* dpPath = (const char*)Marshal::StringToHGlobalAnsi(this->tb_DPPath->Text).ToPointer();
			const char* name = (const char*)Marshal::StringToHGlobalAnsi(this->cb_Names->Text).ToPointer();
			std::string savePath = (const char*)Marshal::StringToHGlobalAnsi(this->tb_ICSPath->Text).ToPointer();
			std::string kwNew = (const char*)Marshal::StringToHGlobalAnsi(this->kw).ToPointer();
			std::string path;

			std::vector<CalendarReminder> reminder;
			for (int i = 0; i < this->t_reminders->Rows->Count; i++)
			{
				System::Data::DataRow^ row = this->t_reminders->Rows[i];
				System::String^ trigger = (System::String^)row->ItemArray[0];
				array<System::String^>^ splitTrigger = trigger->Split(';');
				
				unsigned int num = int::Parse(splitTrigger[0]);
				TriggerType tt;

				switch (int::Parse(splitTrigger[1]))
				{
				case 0:
					tt = TriggerType::tt_minute; break;
				case 1:
					tt = TriggerType::tt_hour; break;
				case 2:
					tt = TriggerType::tt_day; break;
				default:
					tt = TriggerType::tt_hour; break;
				}
				CalendarReminder r = CalendarReminder(num, tt);
				reminder.push_back(r);
			}

			int ret = makeCalendar(dpPath, name, savePath, kwNew, &path, &reminder);
			if (ret > 0)
			{
				this->lb_Debug->Items->Add("Kalender-Datei f�r " + this->cb_Names->Text + " gespeichert unter:");
				this->lb_Debug->Items->Add(gcnew String((const char*)path.c_str()));
				this->lb_Debug->Items->Add(ret.ToString() + " Termine erstellt");
			}
			else
			{
				if (ret == 0) this->lb_Debug->Items->Add("Keine Dienste f�r " + this->cb_Names->Text);
				else
				{
					this->lb_Debug->Items->Add("Datei konnte nicht erstellt werden!");
					this->lb_Debug->Items->Add("Fehlercode: " + ret.ToString());
					this->lb_Debug->Items->Add("Bitte melden an \"martin-oehme@gmx.de\" (oder einfach per WhatsApp)");
				}
			}
		}
		else
		{
			this->lb_Debug->Items->Add("Datei konnte nicht erstellt werden!");
			this->lb_Debug->Items->Add("Bitte rote K�sten korrekt ausf�llen!");
		}
	}
	private: System::Void b_addReminder_Click(System::Object^ sender, System::EventArgs^ e) {
		System::String^ i_data = this->nud_counter->Value.ToString() + ";" + this->cb_TriggerType->SelectedIndex.ToString();
		
		this->lb_reminder->Items->Add(gcnew System::String(this->nud_counter->Value.ToString() + " " + this->cb_TriggerType->Text));

		System::Data::DataRow^ row = this->t_reminders->NewRow();
		row["Reminders"] = i_data;
		this->t_reminders->Rows->Add(row);
	}

	private: System::Void b_delReminder_Click(System::Object^ sender, System::EventArgs^ e) {
		int num = this->lb_reminder->SelectedIndex;
		if (num >= 0)
		{
			this->t_reminders->Rows->RemoveAt(num);
			this->lb_reminder->Items->RemoveAt(num);
		}
	}
};
}
